diff --git a/examples/derive.rs b/examples/derive.rs
index b67aaf3..b134c2a 100644
--- a/examples/derive.rs
+++ b/examples/derive.rs
@@ -1,9 +1,12 @@
+#[cfg(feature = "valuable-derive",)]
 use valuable::Valuable;
 
+#[cfg(feature = "valuable-derive",)]
 use std::collections::HashMap;
 
 // `Debug` not implemented for struct, the debug implementation is going via
 // valuable.
+#[cfg(feature = "valuable-derive",)]
 #[derive(Valuable)]
 struct Person {
     name: String,
@@ -12,6 +15,7 @@ struct Person {
     favorites: HashMap<String, String>,
 }
 
+#[cfg(feature = "valuable-derive",)]
 fn main() {
     let mut p = Person {
         name: "John Doe".to_string(),
@@ -24,3 +28,7 @@ fn main() {
 
     println!("{:#?}", p.as_value());
 }
+
+#[cfg(not(feature = "valuable-derive",))]
+fn main() {
+}
diff --git a/examples/print.rs b/examples/print.rs
index b6998d3..ae1d460 100644
--- a/examples/print.rs
+++ b/examples/print.rs
@@ -1,13 +1,17 @@
+#[cfg(feature = "valuable-derive",)]
 use valuable::{NamedValues, Valuable, Value, Visit};
 
+#[cfg(feature = "valuable-derive",)]
 struct Print(String);
 
+#[cfg(feature = "valuable-derive",)]
 impl Print {
     fn indent(&self) -> Print {
         Print(format!("{}    ", self.0))
     }
 }
 
+#[cfg(feature = "valuable-derive",)]
 impl Visit for Print {
     fn visit_value(&mut self, value: Value<'_>) {
         match value {
@@ -69,6 +73,7 @@ impl Visit for Print {
     }
 }
 
+#[cfg(feature = "valuable-derive",)]
 #[derive(Valuable)]
 struct Person {
     name: String,
@@ -76,6 +81,7 @@ struct Person {
     addresses: Vec<Address>,
 }
 
+#[cfg(feature = "valuable-derive",)]
 #[derive(Valuable)]
 struct Address {
     street: String,
@@ -83,6 +89,7 @@ struct Address {
     zip: String,
 }
 
+#[cfg(feature = "valuable-derive",)]
 fn main() {
     let person = Person {
         name: "Angela Ashton".to_string(),
@@ -104,3 +111,7 @@ fn main() {
     let mut print = Print("".to_string());
     valuable::visit(&person, &mut print);
 }
+
+#[cfg(not(feature = "valuable-derive",))]
+fn main() {
+}
diff --git a/src/enumerable.rs b/src/enumerable.rs
index 3161f19..5521a71 100644
--- a/src/enumerable.rs
+++ b/src/enumerable.rs
@@ -25,6 +25,7 @@ use core::fmt;
 /// [`visit_named_fields()`]: Visit::visit_named_fields
 /// [`visit_unnamed_fields()`]: Visit::visit_unnamed_fields
 ///
+#[cfg_attr(feature = "valuable-derive", doc = r##"
 /// ```
 /// use valuable::{Valuable, Value, Visit};
 ///
@@ -58,6 +59,7 @@ use core::fmt;
 ///
 /// valuable::visit(&my_enum, &mut PrintVariant);
 /// ```
+"##)]
 ///
 /// If the enum is **statically** defined, then all variants, and variant fields
 /// are known ahead of time and may be accessed via the [`EnumDef`] instance
@@ -70,6 +72,7 @@ use core::fmt;
 /// Implementing `Enumerable` is usually done by adding `#[derive(Valuable)]` to
 /// a Rust `enum` definition.
 ///
+#[cfg_attr(feature = "valuable-derive", doc = r##"
 /// ```
 /// use valuable::{Valuable, Enumerable, EnumDef};
 ///
@@ -93,6 +96,7 @@ use core::fmt;
 /// assert_eq!("Foo", variants[0].name());
 /// assert!(variants[0].fields().is_unnamed());
 /// ```
+"##)]
 pub trait Enumerable: Valuable {
     /// Returns the enum's definition.
     ///
@@ -100,6 +104,7 @@ pub trait Enumerable: Valuable {
     ///
     /// # Examples
     ///
+    #[cfg_attr(feature = "valuable-derive", doc = r##"
     /// ```
     /// use valuable::{Enumerable, Valuable};
     ///
@@ -113,12 +118,14 @@ pub trait Enumerable: Valuable {
     ///
     /// assert_eq!("MyEnum", my_enum.definition().name());
     /// ```
+    "##)]
     fn definition(&self) -> EnumDef<'_>;
 
     /// Returns the `enum`'s current variant.
     ///
     /// # Examples
     ///
+    #[cfg_attr(feature = "valuable-derive", doc = r##"
     /// ```
     /// use valuable::{Enumerable, Valuable};
     ///
@@ -131,6 +138,7 @@ pub trait Enumerable: Valuable {
     /// let my_enum = MyEnum::Foo;
     /// assert_eq!("Foo", my_enum.variant().name());
     /// ```
+    "##)]
     fn variant(&self) -> Variant<'_>;
 }
 
@@ -151,6 +159,7 @@ pub enum EnumDef<'a> {
     ///
     /// A statically defined enum
     ///
+    #[cfg_attr(feature = "valuable-derive", doc = r##"
     /// ```
     /// use valuable::{Valuable, Enumerable, EnumDef};
     ///
@@ -174,6 +183,7 @@ pub enum EnumDef<'a> {
     /// assert_eq!("Foo", variants[0].name());
     /// assert_eq!("Bar", variants[1].name());
     /// ```
+    "##)]
     #[non_exhaustive]
     Static {
         /// The enum's name
@@ -305,6 +315,7 @@ impl<'a> EnumDef<'a> {
     ///
     /// # Examples
     ///
+    #[cfg_attr(feature = "valuable-derive", doc = r##"
     /// ```
     /// use valuable::{Enumerable, Valuable};
     ///
@@ -317,6 +328,7 @@ impl<'a> EnumDef<'a> {
     /// let def = Foo::Bar.definition();
     /// assert_eq!("Foo", def.name());
     /// ```
+    "##)]
     pub fn name(&self) -> &str {
         match self {
             EnumDef::Static { name, .. } => name,
@@ -328,6 +340,7 @@ impl<'a> EnumDef<'a> {
     ///
     /// # Examples
     ///
+    #[cfg_attr(feature = "valuable-derive", doc = r##"
     /// ```
     /// use valuable::{Enumerable, Valuable};
     ///
@@ -343,6 +356,7 @@ impl<'a> EnumDef<'a> {
     /// assert_eq!(2, variants.len());
     /// assert_eq!("Bar", variants[0].name());
     /// ```
+    "##)]
     pub fn variants(&self) -> &[VariantDef<'_>] {
         match self {
             EnumDef::Static { variants, .. } => variants,
@@ -356,6 +370,7 @@ impl<'a> EnumDef<'a> {
     ///
     /// With a static enum
     ///
+    #[cfg_attr(feature = "valuable-derive", doc = r##"
     /// ```
     /// use valuable::{Enumerable, Valuable};
     ///
@@ -368,6 +383,7 @@ impl<'a> EnumDef<'a> {
     /// let def = Foo::Bar.definition();
     /// assert!(def.is_static());
     /// ```
+    "##)]
     ///
     /// With a dynamic enum
     ///
@@ -387,6 +403,7 @@ impl<'a> EnumDef<'a> {
     ///
     /// With a static enum
     ///
+    #[cfg_attr(feature = "valuable-derive", doc = r##"
     /// ```
     /// use valuable::{Enumerable, Valuable};
     ///
@@ -399,6 +416,7 @@ impl<'a> EnumDef<'a> {
     /// let def = Foo::Bar.definition();
     /// assert!(!def.is_dynamic());
     /// ```
+    "##)]
     ///
     /// With a dynamic enum
     ///
diff --git a/src/lib.rs b/src/lib.rs
index 5ba6341..777588b 100644
--- a/src/lib.rs
+++ b/src/lib.rs
@@ -11,6 +11,7 @@
 //!
 //! First, derive [`Valuable`][macro@crate::Valuable] on your types.
 //!
+#![cfg_attr(feature = "valuable-derive", doc = r##"
 //! ```
 //! use valuable::Valuable;
 //!
@@ -25,6 +26,7 @@
 //!     Custom(String),
 //! }
 //! ```
+"##)]
 //!
 //! Then, implement a [visitor][Visit] to inspect the data.
 //!
@@ -81,6 +83,7 @@
 //!
 //! Then, use the visitor to visit the value.
 //!
+#![cfg_attr(feature = "valuable-derive", doc = r##"
 //! ```
 //! # use valuable::*;
 //! # #[derive(Valuable)]
@@ -94,6 +97,7 @@
 //! let hello_world = HelloWorld { message: Message::HelloWorld };
 //! hello_world.visit(&mut Print);
 //! ```
+"##)]
 
 #![cfg_attr(not(feature = "std"), no_std)]
 #![cfg_attr(docsrs, feature(doc_cfg, doc_auto_cfg, doc_cfg_hide))]
diff --git a/src/structable.rs b/src/structable.rs
index 611a7e5..45d054e 100644
--- a/src/structable.rs
+++ b/src/structable.rs
@@ -17,6 +17,7 @@ use core::fmt;
 /// may be called multiple times per `Structable`, but the two methods are never
 /// mixed.
 ///
+#[cfg_attr(feature = "valuable-derive", doc = r##"
 /// ```
 /// use valuable::{NamedValues, Valuable, Value, Visit};
 ///
@@ -50,6 +51,7 @@ use core::fmt;
 ///
 /// valuable::visit(&my_struct, &mut PrintFields);
 /// ```
+"##)]
 ///
 /// If the struct is **statically** defined, then all fields are known ahead of
 /// time and may be accessed via the [`StructDef`] instance returned by
@@ -61,6 +63,7 @@ use core::fmt;
 /// Implementing `Structable` is usually done by adding `#[derive(Valuable)]` to
 /// a Rust `struct` definition.
 ///
+#[cfg_attr(feature = "valuable-derive", doc = r##"
 /// ```
 /// use valuable::{Fields, Valuable, Structable, StructDef};
 ///
@@ -86,6 +89,7 @@ use core::fmt;
 ///     _ => unreachable!(),
 /// }
 /// ```
+"##)]
 ///
 /// [`definition()`]: Structable::definition()
 pub trait Structable: Valuable {
@@ -95,6 +99,7 @@ pub trait Structable: Valuable {
     ///
     /// # Examples
     ///
+    #[cfg_attr(feature = "valuable-derive", doc = r##"
     /// ```
     /// use valuable::{Structable, Valuable};
     ///
@@ -108,6 +113,8 @@ pub trait Structable: Valuable {
     /// };
     ///
     /// assert_eq!("MyStruct", my_struct.definition().name());
+    /// ```
+    "##)]
     fn definition(&self) -> StructDef<'_>;
 }
 
@@ -129,6 +136,7 @@ pub enum StructDef<'a> {
     ///
     /// A statically defined struct
     ///
+    #[cfg_attr(feature = "valuable-derive", doc = r##"
     /// ```
     /// use valuable::{Fields, Valuable, Structable, StructDef};
     ///
@@ -154,6 +162,7 @@ pub enum StructDef<'a> {
     ///     _ => unreachable!(),
     /// }
     /// ```
+    "##)]
     #[non_exhaustive]
     Static {
         /// The struct's name.
diff --git a/src/value.rs b/src/value.rs
index 2db9cd1..089d52f 100644
--- a/src/value.rs
+++ b/src/value.rs
@@ -33,6 +33,7 @@ macro_rules! value {
         ///
         /// Converting a struct
         ///
+        #[cfg_attr(feature = "valuable-derive", doc = r##"
         /// ```
         /// use valuable::{Value, Valuable};
         ///
@@ -55,6 +56,7 @@ macro_rules! value {
         ///     format!("{:?}", hello),
         /// );
         /// ```
+	"##)]
         ///
         /// [visitor]: Visit
         #[non_exhaustive]
@@ -359,6 +361,7 @@ value! {
     ///
     /// # Examples
     ///
+    #[cfg_attr(feature = "valuable-derive", doc = r##"
     /// ```
     /// use valuable::{Value, Valuable};
     ///
@@ -373,12 +376,14 @@ value! {
     ///
     /// let v = Value::Structable(&my_struct);
     /// ```
+    "##)]
     Structable(&'a dyn Structable),
 
     /// A Rust enum value
     ///
     /// # Examples
     ///
+    #[cfg_attr(feature = "valuable-derive", doc = r##"
     /// ```
     /// use valuable::{Value, Valuable};
     ///
@@ -391,6 +396,7 @@ value! {
     /// let my_enum = MyEnum::Foo;
     /// let v = Value::Enumerable(&my_enum);
     /// ```
+    "##)]
     Enumerable(&'a dyn Enumerable),
 
     /// A tuple value
@@ -627,6 +633,7 @@ macro_rules! convert {
             ///
             /// # Examples
             ///
+            #[cfg_attr(feature = "valuable-derive", doc = r##"
             /// ```
             /// use valuable::{Value, Valuable};
             ///
@@ -640,6 +647,7 @@ macro_rules! convert {
             /// assert!(Value::Structable(&hello).as_structable().is_some());
             /// assert!(Value::Bool(true).as_structable().is_none());
             /// ```
+	    "##)]
             pub fn as_structable(&self) -> Option<&dyn Structable> {
                 match *self {
                     Value::Structable(v) => Some(v),
@@ -651,6 +659,7 @@ macro_rules! convert {
             ///
             /// # Examples
             ///
+            #[cfg_attr(feature = "valuable-derive", doc = r##"
             /// ```
             /// use valuable::{Value, Valuable};
             ///
@@ -665,6 +674,7 @@ macro_rules! convert {
             /// assert!(Value::Enumerable(&greet).as_enumerable().is_some());
             /// assert!(Value::Bool(true).as_enumerable().is_none());
             /// ```
+	    "##)]
             pub fn as_enumerable(&self) -> Option<&dyn Enumerable> {
                 match *self {
                     Value::Enumerable(v) => Some(v),
diff --git a/src/visit.rs b/src/visit.rs
index f91a04c..64e0994 100644
--- a/src/visit.rs
+++ b/src/visit.rs
@@ -13,6 +13,7 @@ use crate::*;
 ///
 /// Recursively printing a Rust value.
 ///
+#[cfg_attr(feature = "valuable-derive", doc = r##"
 /// ```
 /// use valuable::{NamedValues, Valuable, Value, Visit};
 ///
@@ -119,6 +120,7 @@ use crate::*;
 /// let mut print = Print("".to_string());
 /// valuable::visit(&person, &mut print);
 /// ```
+"##)]
 pub trait Visit {
     /// Visit a single value.
     ///
@@ -189,6 +191,7 @@ pub trait Visit {
     ///
     /// Visiting all fields in a struct.
     ///
+    #[cfg_attr(feature = "valuable-derive", doc = r##"
     /// ```
     /// use valuable::{NamedValues, Valuable, Value, Visit};
     ///
@@ -222,6 +225,7 @@ pub trait Visit {
     ///
     /// valuable::visit(&my_struct, &mut Print);
     /// ```
+    "##)]
     fn visit_named_fields(&mut self, named_values: &NamedValues<'_>) {
         let _ = named_values;
     }
@@ -239,6 +243,7 @@ pub trait Visit {
     ///
     /// Visiting all fields in a struct.
     ///
+    #[cfg_attr(feature = "valuable-derive", doc = r##"
     /// ```
     /// use valuable::{Valuable, Value, Visit};
     ///
@@ -266,6 +271,7 @@ pub trait Visit {
     ///
     /// valuable::visit(&my_struct, &mut Print);
     /// ```
+    "##)]
     fn visit_unnamed_fields(&mut self, values: &[Value<'_>]) {
         let _ = values;
     }
@@ -415,6 +421,7 @@ deref! {
 /// extracted from a struct, it is preferable to obtain the associated
 /// [`NamedField`] once and use it repeatedly.
 ///
+#[cfg_attr(feature = "valuable-derive", doc = r##"
 /// ```
 /// use valuable::{NamedValues, Valuable, Value, Visit};
 ///
@@ -452,6 +459,7 @@ deref! {
 ///
 /// assert_eq!(123, get_foo.0);
 /// ```
+"##)]
 ///
 /// [`Visit`]: Visit [`NamedField`]: crate::NamedField
 pub fn visit(value: &impl Valuable, visit: &mut dyn Visit) {
